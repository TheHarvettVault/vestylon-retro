# 
#  __   __  __ _____    --|| About ||--
#  \ \ / / / _|  _  |   Vestylon Retro 20220520.01
#   \ V /_/ / | | | | 
#    \_/|__/  |_|< <    by Jayvee Enaguas (The Harvett Vault)
#                 \_\      <harvettfox96@tilde.club>
# 
#  A retro-styled theme in the VGA palette for IceWM on *nix system.
# 

# --|| Metadata ||--
ThemeName="Vestylon Retro"
ThemeAuthor="Jayvee Enaguas (The Harvett Vault)"
ThemeDescription="A retro-styled theme in the VGA palette for IceWM on *nix system."
License="Unlicense"

# --|| Appearance ||--

# ----|| Widget ||---

# ------|| General ||------
# Option:
Look=pixmap
# Colour:
ColorDialog=#AAAAAA
ColorLabel=#AAAAAA
ColorLabelText=black
# Text:
LabelFontNameXft="Modern DOS 8x16-12"

# ------|| Button ||------
# Colour:
ColorNormalButton=#AAAAAA
ColorNormalButtonText=black
ColorActiveButton=#FF55FF
ColorNormalButtonText=black
# Text:
NormalButtonFontNameXft="Modern DOS 8x16-12"
ActiveButtonFontNameXft="Modern DOS 8x16-12"

# ------|| Text box ||------
# Colour:
ColorInput=white
ColorInputText=black
ColorInputSelection=#55FF55
ColorInputSelectionText=black
# Text:
InputFontNameXft="Modern DOS 8x16-12"

# ------|| List box ||------
# Colour:
ColorListBox=white
ColorListBoxText=black
ColorListBoxSelection=#55FF55
ColorListBoxSelectionText=black
# Text:
ListBoxFontNameXft="Modern DOS 8x16-12"

# ------|| Scroll bar ||------
# Geometry:
ScrollBarX=15
ScrollBarY=15
# Colour:
ColorScrollBar=#AAAAAA
ColorScrollBarSlider=#AAAAAA
ColorScrollBarButton=#AAAAAA
ColorScrollBarButtonArrow=black
ColorScrollBarInactiveArrow=#555555

# ----|| Menu ||----
# Colour:
ColorNormalMenu=#AAAAAA
ColorNormalMenuItemText=black
ColorActiveMenuItem=#55FF55
ColorDisabledMenuItemText=#555555
# Text:
MenuFontNameXft="Modern DOS 8x16-12"

# ----|| Tool-tip ||----
# Colour:
ColorToolTip=#FFFF55
ColorToolTipText=black
# Text:
ToolTipFontNameXft="Modern DOS 8x16-12"

# ----|| Window move and resize indicator ||----
# Colour:
ColorMoveSizeStatus=#AAAAAA
ColorMoveSizeStatusText=black
# Font:
StatusFontNameXft="Modern DOS 8x16-12"

# ----|| Tab switcher ||----
# Colour:
ColorQuickSwitch=#AAAAAA
ColorQuickSwitchText=black
ColorQuickSwitchActive=#555555
# Font:
QuickSwitchFontNameXft="Modern DOS 8x16-12"

# ----|| Window ||----
# Option:
ShowMenuButtonIcon=0
# Placement:
TitleButtonsSupported="imrsx"
TitleButtonsLeft="xs"
TitleButtonsRight="mri"
# Geometry:
TitleBarHeight=17
BorderSizeX=4
BorderSizeY=4
DlgBorderSizeX=1
DlgBorderSizeY=1
# Colour:
ColorNormalTitleBar=white
ColorNormalTitleBarText=#555555
ColorNormalBorder=white
ColorActiveTitleBar=#55FF55
ColorActiveTitleBarText=white
ColorActiveBorder=white
# Text:
TitleBarJustify=50
TitleFontNameXft="Modern DOS 8x16-12"

# ----|| Panel ||----

# ------|| General ||------
# Colour:
ColorDefaultTaskBar=#AAAAAA
ColorNormalTaskBarApp=#AAAAAA
ColorNormalTaskBarAppText=black
ColorActiveTaskBarApp=#55FF55
ColorActiveTaskBarAppText=black
ColorMinimizedTaskBarApp=#AAAAAA
ColorMinimizedTaskBarAppText=black
ColorInvisibleTaskBarApp=#AAAAAA
ColorInvisibleTaskBarAppText=#555555
# Text:
NormalTaskBarFontNameXft="Modern DOS 8x16-12"
ActiveTaskBarFontNameXft="Modern DOS 8x16-12"
MinimizedWindowFontNameXft="Modern DOS 8x16-12"

# ------|| Clock ||------
# Colour:
ColorClock=#AAAAAA
ColorClockText=black
# Text:
ClockFontNameXft="Modern DOS 8x16-12"

# ------|| Workspace ||------
# Colour:
ColorActiveWorkspaceButton=#FFFF55
# Text:
NormalWorkspaceFontNameXft="Modern DOS 8x16-12"
ActiveWorkspaceFontNameXft="Modern DOS 8x16-12"
